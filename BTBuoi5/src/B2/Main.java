package B2;

import java.util.Scanner;

public class Main {
    public static void menu(){
        System.out.println("1.Rút gọn phân số");
        System.out.println("2.Nghịch đảo phân số");
        System.out.println("3.Cộng 2 phân số");
        System.out.println("4.Trừ 2 phân số");
        System.out.println("5.Nhân 2 phân số");
        System.out.println("6.Chia 2 phân số");
        System.out.println("7.So sánh 2 phân số");
        System.out.println("8.Thoát");
        System.out.print("Lựa chọn: ");
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int chon=0;
        int numer,denom;
        while (chon!=8){
            menu();
            chon = sc.nextInt();
            switch (chon){
                case 1 :
                    System.out.println("Nhập tử số: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số: ");
                    denom = sc.nextInt();
                    Rational r = new Rational(numer,denom);
                    r.reduce();
                    System.out.println("Kết quả: "+r.getR_numer()+"/"+r.getR_denom());
                    sc.nextLine();
                    break;
                case 2 :
                    System.out.println("Nhập tử số: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số: ");
                    denom = sc.nextInt();
                    Rational x = new Rational(numer,denom);
                    x.reciprocal();
                    System.out.println("Kết quả: "+x.getR_numer()+"/"+x.getR_denom());
                    sc.nextLine();
                    break;
                case 3 :
                    System.out.println("Nhập tử số thứ 1: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số thứ 1: ");
                    denom = sc.nextInt();
                    Rational y = new Rational(numer,denom);
                    System.out.println("Nhập tử số phân số thứ 2: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số phân số thứ 2: ");
                    denom = sc.nextInt();
                    y.addRational(numer,denom);
                    sc.nextLine();
                    break;
                case 4 :
                    System.out.println("Nhập tử số: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số: ");
                    denom = sc.nextInt();
                    Rational z = new Rational(numer,denom);
                    System.out.println("Nhập tử số phân số trừ: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số phân số trừ: ");
                    denom = sc.nextInt();
                    z.subtractRational(numer,denom);
                    sc.nextLine();
                    break;
                case 5 :
                    System.out.println("Nhập tử số thứ 1: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số thứ 1: ");
                    denom = sc.nextInt();
                    Rational t = new Rational(numer,denom);
                    System.out.println("Nhập tử số phân số thứ 2: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số phân số thứ 2: ");
                    denom = sc.nextInt();
                    t.multiplyRational(numer,denom);
                    sc.nextLine();
                    break;
                case 6 :
                    System.out.println("Nhập tử số: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số: ");
                    denom = sc.nextInt();
                    Rational q = new Rational(numer,denom);
                    System.out.println("Nhập tử số phân số chia: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số phân số chia: ");
                    denom = sc.nextInt();
                    q.divRational(numer,denom);
                    sc.nextLine();
                    break;
                case 7 :
                    System.out.println("Nhập tử số thứ 1: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số thứ 1: ");
                    denom = sc.nextInt();
                    Rational w = new Rational(numer,denom);
                    System.out.println("Nhập tử số phân số thứ 2: ");
                    numer = sc.nextInt();
                    System.out.println("Nhập mẫu số phân số thứ 2: ");
                    denom = sc.nextInt();
                    w.compare(numer,denom);
                    sc.nextLine();
                    break;

            }
        }

    }
}
