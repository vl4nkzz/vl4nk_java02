package B2;

public class Rational {
    private static int r_numer;
    private static int r_denom;

    public Rational(int r_numer, int r_denom) {
        Rational.r_numer=r_numer;
        Rational.r_denom=r_denom;
    }

    public static int getR_numer() {
        return r_numer;
    }

    public static void setR_numer(int r_numer) {
        Rational.r_numer = r_numer;
    }

    public static int getR_denom() {
        return r_denom;
    }

    public static void setR_denom(int r_denom) {
        Rational.r_denom = r_denom;
    }

    private static int findUCLN(int a, int b){
        int i;
        if(a>=b){
            for(i=b;i>1;i--){
                if ((a%i==0) && (b%i==0)) return i;
            }
            return 1;
        }
        else{
            for(i=a;i>1;i--){
                if((b%i==0) && (a%i==0)) return i;
            }
            return 1;
        }
    }

    private static int findBCNN(int a,int b){
        int i;
        if(a>=b){
            for(i=a;i<=(a*b);i++){
                if ((i%a==0) && (i%b==0)) return i;
            }

        }
        else{
            for(i=b;i<=(a*b);i++){
                if((i%b==0) && (i%a==0)) return i;
            }

        }
    return i;
    }

    public static void reduce(){
        int a=findUCLN(r_numer,r_denom);
        while(a!=1){
            r_numer=r_numer/a;
            r_denom=r_denom/a;
            a=findUCLN(r_numer,r_denom);
        }
    }

    public static void reciprocal(){
        int i;
        i = r_numer;
        r_numer = r_denom;
        r_denom = i;

    }

    public static void addRational(int n_numer, int n_denom){
        int kq_numer;
        int kq_denom;
        kq_denom=findBCNN(r_denom, n_denom);
        kq_numer = r_numer*(kq_denom/r_denom)+ n_numer*(kq_denom/n_denom);
        System.out.println("Kết quả: "+kq_numer+"/"+kq_denom);
    }

    public static void subtractRational(int n_numer, int n_denom){
        int kq_numer;
        int kq_denom;
        kq_denom=findBCNN(r_denom, n_denom);
        kq_numer = r_numer*(kq_denom/r_denom) - n_numer*(kq_denom/n_denom);
        System.out.println("Kết quả: "+kq_numer+"/"+kq_denom);
    }

    public static void multiplyRational(int n_numer, int n_denom){
        int kq_numer;
        int kq_denom;
        kq_denom=r_denom * n_denom;
        kq_numer = r_numer*n_numer;
        System.out.println("Kết quả: "+kq_numer+"/"+kq_denom);
    }

    public static void divRational(int n_numer, int n_denom){
        int kq_numer;
        int kq_denom;
        kq_denom=r_denom * n_numer;
        kq_numer = r_numer*n_denom;
        System.out.println("Kết quả: "+kq_numer+"/"+kq_denom);
    }

    public static void compare(int n_numer, int n_denom){

        if (((double)Math.round((double)(r_numer/r_denom)*10000.0)/10000)>((double)Math.round((double)(n_numer/n_denom)*10000.0)/10000)){
            System.out.println("Lớn hơn");
        }
        else if (((double)Math.round((double)(r_numer/r_denom)*10000.0)/10000)<((double)Math.round((double)(n_numer/n_denom)*10000.0)/10000))
            System.out.println("Nhỏ hơn");
        else System.out.println("Bằng nhau");
    }
}
