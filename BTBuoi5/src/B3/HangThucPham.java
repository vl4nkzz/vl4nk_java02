package B3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HangThucPham {
    private int maHang;
    private String tenHang;
    private int donGia;
    private String ngaysx;
    private String ngaysd;

    public HangThucPham(int maHang, String tenHang, int donGia, String ngaysx, String ngaysd) {
        this.maHang = maHang;
        this.tenHang = tenHang;
        this.donGia = donGia;
        this.ngaysx = ngaysx;
        this.ngaysd = ngaysd;
    }

    public int getMaHang() {
        return maHang;
    }

    public void setMaHang(int maHang) {
        this.maHang = maHang;
    }

    public String getTenHang() {
        return tenHang;
    }

    public void setTenHang(String tenHang) {
        this.tenHang = tenHang;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public String getNgaysx() {
        return ngaysx;
    }

    public void setNgaysx(String ngaysx) {
        this.ngaysx = ngaysx;
    }

    public String getNgaysd() {
        return ngaysd;
    }

    public void setNgaysd(String ngaysd) {
        this.ngaysd = ngaysd;
    }
    public boolean dahethan() throws ParseException {
        Calendar cal = Calendar.getInstance();
        Date now = (Date) cal.getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        Date ngaysd = df.parse(this.ngaysd);
        long tru = ngaysd.getTime() - now.getTime();
        if (tru < 0)
            return true;
        return false;
    }

    }
}
