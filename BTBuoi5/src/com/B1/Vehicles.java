package com.B1;

public class Vehicles {
    private int value;
    private int vol;
    private double taxOf;

    public Vehicles(int value, int vol) {
        this.vol = vol;
        this.value=value;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getVol() {
        return this.vol;
    }

    public void setVol(int vol) {
        this.vol = vol;
    }

    public double getTaxOf() {
        if (this.vol<100)
            this.taxOf = this.value*0.01;
        if ((this.vol>=100) && (this.vol<=200))
            this.taxOf = this.value*0.03;
        if (this.vol>200)
            this.taxOf = this.value*0.05;
        return this.taxOf;

    }


}

